package com.ogaclejapan.textprogressbar;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class TextProgressBar extends ProgressBar {

  private static final String DEFAULT_TEXT = "Progress...";
  private static final int DEFAULT_TEXT_SIZE = 48;
  private static final int DEFAULT_TEXT_COLOR = Color.BLACK;
  private static final int DEFAULT_DURATION = 1500;

  public TextProgressBar(Context context) {
    this(context, null);
  }

  public TextProgressBar(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public TextProgressBar(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

    setIndeterminate(true);

    TextProgressDrawable.Builder tpd = new TextProgressDrawable.Builder(context);

    if (isInEditMode()) {
      setIndeterminateDrawable(tpd.build());
      return;
    }

    if (attrs != null) {

      TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.tpb_TextProgressBar);

      String text = a.getString(R.styleable.tpb_TextProgressBar_tpb_progressText);
      if (!TextUtils.isEmpty(text)) {
        tpd.setText(text);
      }

      float textSize = a
          .getDimension(R.styleable.tpb_TextProgressBar_tpb_progressTextSize, Float.NaN);
      if (!Float.isNaN(textSize)) {
        tpd.setTextSize(textSize);
      }

      int textColor = a.getColor(R.styleable.tpb_TextProgressBar_tpb_progressTextColor, -1);
      if (textColor != -1) {
        tpd.setTextColor(textColor);
      }

      int duration = a.getInteger(R.styleable.tpb_TextProgressBar_tpb_progressDuration, -1);
      if (duration != -1) {
        tpd.setDuration(duration);
      }

      a.recycle();

    }

    setIndeterminateDrawable(tpd.build());
  }

  @Override
  protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    final Drawable d = getIndeterminateDrawable();

    int w = MeasureSpec.getSize(widthMeasureSpec);
    int h = MeasureSpec.getSize(heightMeasureSpec);

    if (d != null && MeasureSpec.getMode(widthMeasureSpec) != MeasureSpec.EXACTLY) {
      w = d.getIntrinsicWidth();
    }

    if (d != null && MeasureSpec.getMode(heightMeasureSpec) != MeasureSpec.EXACTLY) {
      h = d.getIntrinsicHeight();
    }

    setMeasuredDimension(w, h);
  }

  public boolean isRunning() {
    return TextProgressDrawable.class.cast(getIndeterminateDrawable()).isRunning();
  }

  public static class TextProgressDrawable extends Drawable
      implements Animatable, ValueAnimator.AnimatorUpdateListener {

    private final int maxAlpha;

    private final Rect rect;

    private final ValueAnimator alphaAnimator;

    private final int textWidth;

    private final int textHeight;

    private final String text;

    private final TextPaint textPaint;

    private boolean isRunning;

    private TextProgressDrawable(Builder builder) {
      textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
      textPaint.setStyle(Paint.Style.FILL);
      textPaint.setColor(builder.textColor);
      textPaint.setTextSize(builder.textSize);

      Paint.FontMetrics fm = textPaint.getFontMetrics();
      text = builder.text;
      textHeight = Math.round(fm.descent - fm.ascent);
      textWidth = Math.round(textPaint.measureText(text, 0, text.length()));

      maxAlpha = textPaint.getAlpha();
      rect = new Rect();

      alphaAnimator = ValueAnimator.ofFloat(1f, 0f, 1f);
      alphaAnimator.setDuration(builder.duration);
      alphaAnimator.setRepeatCount(ValueAnimator.INFINITE);
      alphaAnimator.addUpdateListener(this);
    }

    @Override
    public void start() {
      if (isRunning()) {
        return;
      }
      isRunning = true;
      alphaAnimator.start();
      invalidateSelf();
    }

    @Override
    public void stop() {
      if (!isRunning()) {
        return;
      }
      isRunning = false;
      alphaAnimator.cancel();
      invalidateSelf();
    }

    @Override
    public boolean isRunning() {
      return isRunning;
    }

    @Override
    public int getIntrinsicWidth() {
      return textWidth;
    }

    @Override
    public int getIntrinsicHeight() {
      return textHeight;
    }

    @Override
    public void draw(Canvas canvas) {

      if (!isRunning()) {
        return;
      }

      float w = Math.max(rect.right, textWidth);
      float h = Math.max(rect.bottom, textHeight);

      float x = (w - textWidth) / 2f;
      float y = ((h - textHeight) / 2f) - textPaint.ascent();

      canvas.drawText(text, x, y, textPaint);
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
      super.onBoundsChange(bounds);
      rect.set(bounds);
    }

    @Override
    public void setAlpha(int alpha) {
      textPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
      textPaint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
      return PixelFormat.TRANSLUCENT;
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
      float value = (Float) animation.getAnimatedValue();
      setAlpha(Math.round(maxAlpha * value));
      invalidateSelf();
    }

    public static class Builder {

      private String text = DEFAULT_TEXT;

      private float textSize = DEFAULT_TEXT_SIZE;

      private int textColor = DEFAULT_TEXT_COLOR;

      private int duration = DEFAULT_DURATION;

      public Builder(Context context) {
        textSize = context.getResources().getDisplayMetrics().density * textSize;
      }

      public Builder setText(String text) {
        this.text = text;
        return this;
      }

      public Builder setTextSize(float size) {
        textSize = size;
        return this;
      }

      public Builder setTextColor(int color) {
        textColor = color;
        return this;
      }

      public Builder setDuration(int duration) {
        this.duration = duration;
        return this;
      }

      public TextProgressDrawable build() {
        return new TextProgressDrawable(this);
      }

    }

  }

}